import datetime
import codecs
from tabulate import tabulate

from ClassPerson import Person


def main():
    separator = '\n' + '-' * 30 + '\n'
    globals_ = globals()

    for i in range(1, 3):
        print(separator)
        globals_.get(f'task{i}')()


def task1():
    test = Person('Гаврищук', 'Вікторія',
                  datetime.datetime.strptime(str(input('Введіть дату [РР-ММ-ДД] -> ')), "%Y-%m-%d").date(),
                  'vika70147')

    print(f"Вік контакту: {test.get_age()} років")
    print(f"Псевдонім контакту: {test.nickname}")
    print(test.get_fullname())


def task2():
    with codecs.open('files\\persons.txt', 'w', "utf-8") as fileText:
        table = [['Гаврищук', 'Вікторія', '2003-10-07'],
                 ['Ковтонюк', 'Анастасія', '2004-07-05'],
                 ['Поліщук', 'Карина', '2004-05-18']]
        fileText.writelines(tabulate(table, headers=["Surname", "Name", "Birth date"]))
    persons = Person.modifier('persons')


if __name__ == "__main__":
    main()
